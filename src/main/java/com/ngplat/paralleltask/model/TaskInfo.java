/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.model
 * @filename: TaskInfo.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.model;

import java.util.Arrays;
import java.util.Date;

import com.ngplat.paralleltask.common.Cleanable;
import com.ngplat.paralleltask.constants.TaskConsts;
import com.ngplat.paralleltask.constants.TaskState;

/**
 * @typename: TaskInfo
 * @brief:
 *         <p>
 *         任务基本信息: 任务Id、任务名称、所依赖父节点、 依赖子节点 
 *         所谓依赖关系, 是指一个节点未结束, 另一个节点不可执行, 涵盖了孤立节点
 *         </p>
 * @author: KI ZCQ
 * @date: 2018年3月9日 下午2:22:24
 * @version: 1.0.0
 * @since
 * 
 */
public class TaskInfo implements Cleanable {
	
	// 任务Id
	private String taskId;
	// 任务名称
	private String taskName = TaskConsts.DEFAULT_TASK_NAME;
	// 所属作业Id
	private String jobId;
	// 完整报名 + 类名
	private String beanClass;
	
	// 任务执行状态
	private TaskState state = TaskState.TASK_INITIAL;
	// 任务执行优先级
	private int priority = TaskConsts.LOWEST_PRECEDENCE;
	// 任务状态(启用/废弃)
	private String status = TaskConsts.ENABLE_STATUS;

	// Task创建时间
	private Date createTime;
	// Task开始执行时间
	private Date beginTime;
	// Task执行完成时间
	private Date finishTime;

	// 所依赖的父节点Id
	private String[] parentIds;

	/**
	 * 创建一个新的实例 TaskInfo.
	 */
	public TaskInfo() {
		// 任务创建时间
		createTime = new Date();
	}
	
	/**
	 * 创建一个新的实例 TaskInfo.
	 * @param taskId
	 */
	public TaskInfo(String taskId) {
		this();
		setTaskId(taskId);
	}
	
	/**
	 * 创建一个新的实例 TaskInfo.
	 * @param taskId
	 */
	public TaskInfo(String taskId, String taskName) {
		this(taskId);
		this.taskName = taskName;
	}

	/**
	 * taskId
	 *
	 * @return the taskId
	 * @since 1.0.0
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId
	 *            the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**
	 * taskName
	 *
	 * @return the taskName
	 * @since 1.0.0
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * @param taskName
	 *            the taskName to set
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * createTime
	 *
	 * @return the createTime
	 * @since 1.0.0
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * beginTime
	 *
	 * @return the beginTime
	 * @since 1.0.0
	 */
	public Date getBeginTime() {
		return beginTime;
	}

	/**
	 * @param beginTime
	 *            the beginTime to set
	 */
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	/**
	 * finishTime
	 *
	 * @return the finishTime
	 * @since 1.0.0
	 */
	public Date getFinishTime() {
		return finishTime;
	}

	/**
	 * @param finishTime
	 *            the finishTime to set
	 */
	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * state
	 *
	 * @return  the state
	 * @since   1.0.0
	*/
	public TaskState getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(TaskState state) {
		this.state = state;
	}
	
	/**
	 * priority
	 *
	 * @return  the priority
	 * @since   1.0.0
	*/
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * status
	 *
	 * @return  the status
	 * @since   1.0.0
	*/
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * parentIds
	 *
	 * @return  the parentIds
	 * @since   1.0.0
	*/
	public String[] getParentIds() {
		return parentIds;
	}

	/**
	 * @param parentIds the parentIds to set
	 */
	public void setParentIds(String[] parentIds) {
		this.parentIds = parentIds;
	}

	/**
	 * jobId
	 *
	 * @return  the jobId
	 * @since   1.0.0
	*/
	public String getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * beanClass
	 *
	 * @return  the beanClass
	 * @since   1.0.0
	*/
	public String getBeanClass() {
		return beanClass;
	}

	/**
	 * @param beanClass the beanClass to set
	 */
	public void setBeanClass(String beanClass) {
		this.beanClass = beanClass;
	}
	

	/** 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TaskInfo [taskId=" + taskId + ", taskName=" + taskName + ", jobId=" + jobId + ", beanClass=" + beanClass
				+ ", state=" + state + ", priority=" + priority + ", status=" + status + ", createTime=" + createTime
				+ ", beginTime=" + beginTime + ", finishTime=" + finishTime + ", parentIds="
				+ Arrays.toString(parentIds) + "]";
	}

	/** 
	 * @see com.ngplat.paralleltask.common.Cleanable#cleanResource()
	 */
	@Override
	public void cleanResource() {
		// 任务执行状态
		this.state = TaskState.TASK_INITIAL;
		// Task开始执行时间
		this.beginTime = null;
		// Task执行完成时间
		this.finishTime = null;
	}
	
}
