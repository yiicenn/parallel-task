/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.task
 * @filename: TaskExecutionResult.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.task;

import java.util.Map;

import com.ngplat.paralleltask.common.Cleanable;

/**
 * @typename: TaskExecutionResult
 * @brief: 执行结果上下文
 * @author: KI ZCQ
 * @date: 2018年5月29日 上午9:35:18
 * @version: 1.0.0
 * @since
 * 
 */
public interface TaskExecutionResult extends Cleanable {

	/**
	 * @Description: 结果数据存储
	 * @param key
	 * @param value
	 */
	<E> void putResult(String key, E value);

	/**
	 * @Description: 获取并行执行的数据结果
	 * @param taskName
	 * @return 对应的数据结果
	 */
	<E> E getResult(String taskName);

	/**
	 * @Description: 获取所有结果
	 * @return 获取所有执行结果
	 */
	Map<String, Object> getResult();

}
