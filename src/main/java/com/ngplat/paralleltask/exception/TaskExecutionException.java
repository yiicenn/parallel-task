/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.exception
 * @filename: TaskExecutionException.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.exception;

/**
 * @typename: TaskExecutionException
 * @brief: 自定义异常
 * @author: KI ZCQ
 * @date: 2018年5月23日 下午6:03:34
 * @version: 1.0.0
 * @since
 *
 */
public class TaskExecutionException extends RuntimeException {

	private static final long serialVersionUID = -7375423850222016116L;

	public TaskExecutionException(String msg) {
		super(msg);
	}

	public TaskExecutionException(Throwable cause) {
		super(cause);
	}

	public TaskExecutionException(String message, Throwable cause) {
		super(message, cause);
	}

	public Throwable getUnderlyingException() {
		return super.getCause();
	}

	@Override
	public String toString() {
		Throwable cause = getUnderlyingException();
		if (cause == null || cause == this) {
			return super.toString();
		} else {
			return super.toString() + " [See nested exception: " + cause + "]";
		}
	}
}
