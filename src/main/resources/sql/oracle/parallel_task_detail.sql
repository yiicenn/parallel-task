/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.1.0.7.0

Source Server         : mtsdb
Source Server Version : 100200
Source Host           : 192.168.22.72:1521
Source Schema         : MTS

Target Server Type    : ORACLE
Target Server Version : 100200
File Encoding         : 65001

Date: 2018-06-06 12:45:57
*/

-- ----------------------------
-- Table structure for PARALLEL_TASK_DETAIL
-- ----------------------------
create table PARALLEL_TASK_DETAIL
(
  task_id     VARCHAR2(64) not null,
  task_name   VARCHAR2(200),
  parent_ids  VARCHAR2(1000),
  job_id      VARCHAR2(64),
  job_name    VARCHAR2(100),
  bean_class  VARCHAR2(1000) not null,
  task_status VARCHAR2(1) default 'N' not null,
  priority    NUMBER(4) default 100 not null,
  task_desc   VARCHAR2(250),
  create_date DATE,
  update_date DATE
);
-- Add comments to the table 
comment on table PARALLEL_TASK_DETAIL
  is '任务信息详情表';
-- Add comments to the columns 
comment on column PARALLEL_TASK_DETAIL.task_id
  is '任务ID';
comment on column PARALLEL_TASK_DETAIL.task_name
  is '任务名称';
comment on column PARALLEL_TASK_DETAIL.parent_ids
  is '所有依赖任务ID, 以","作为分隔符';
comment on column PARALLEL_TASK_DETAIL.job_id
  is '所属作业ID';
comment on column PARALLEL_TASK_DETAIL.job_name
  is '所属作业名称';
comment on column PARALLEL_TASK_DETAIL.bean_class
  is '任务对应的业务处理器(包名+类名)';
comment on column PARALLEL_TASK_DETAIL.task_status
  is '任务状态值(N-正常, D-废弃)';
comment on column PARALLEL_TASK_DETAIL.priority
  is '优先级';
comment on column PARALLEL_TASK_DETAIL.task_desc
  is '任务简单描述';
comment on column PARALLEL_TASK_DETAIL.create_date
  is '任务创建日期';
comment on column PARALLEL_TASK_DETAIL.update_date
  is '任务更新日期';
-- Create/Recreate primary, unique and foreign key constraints 
alter table PARALLEL_TASK_DETAIL
  add constraint PARALLEL_TASK_DETAIL_PK unique (TASK_ID);
-- Create/Recreate check constraints 
alter table PARALLEL_TASK_DETAIL
  add constraint PARALLEL_TASK_CK_STATUS
  check (task_status in ('N', 'D'));