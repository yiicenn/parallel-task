/**
 * @project: parallel-task
 * @package: com.ngplat.paralleltask.test.spring
 * @filename: SubTask.java
 *
 * Copyright (c) 2018 eSunny Info. Tech Ltd. All rights reserved.
 * 
 */
package com.ngplat.paralleltask.test.spring;

import com.ngplat.paralleltask.annotation.TaskBean;
import com.ngplat.paralleltask.exception.TaskExecutionException;
import com.ngplat.paralleltask.task.TaskContext;
import com.ngplat.paralleltask.task.TaskProcessor;

/** 
 * @typename: SubTask
 * @brief: 〈一句话功能简述〉
 * @author: KI ZCQ
 * @date: 2018年5月26日 下午3:44:18
 * @version: 1.0.0
 * @since
 * 
 */
@TaskBean(taskId = "2", name = "SubTask")
public class SubTask extends TaskProcessor {

	// serialVersionUID
	private static final long serialVersionUID = 6942288734182114751L;

	/**
	 * 创建一个新的实例 SubTask.
	 */
	public SubTask() {
		// TODO Auto-generated constructor stub
	}

	/** 
	 * @see com.ngplat.paralleltask.task.TaskProcessor#runWorker(com.ngplat.paralleltask.task.TaskContext)
	 */
	@Override
	public void runWorker(TaskContext context) throws TaskExecutionException {
		System.out.println(String.format("[SubTask] runWorker, result: %d", (10 - 8)));
	}

}
